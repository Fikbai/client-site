<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Kelas;
use App\Model\Jadwal;
use Illuminate\Support\Facades\Http;

class KelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = Http::withToken(session()->get('tokenUser'))
                        ->get(env('REST_API_ENDPOINT').'/api/kelas');

        $dataResponse = json_decode($response);

        return view('kelas.index', [
            'dataKelas' => $dataResponse
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $response = Http::withToken(session()->get('tokenUser'))
                        ->get(env('REST_API_ENDPOINT').'/api/kelas');

        $dataResponse = json_decode($response);

        return view('kelas.create', [
            'dataKelas' => $dataResponse
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = Http::withToken(session()->get('tokenUser'))->post(env('REST_API_ENDPOINT').'/api/kelas', [
            'kode_kelas' => $request->kode_kelas,
            'nama_kelas' => $request->nama_kelas,
        ]);

        $dataResponse = json_decode($response);
        if($dataResponse->status == false){
            foreach($dataResponse->message as $msg){
                return redirect()->route('kelas.create')->with('warning', $msg);
            }
        }

        return redirect()->route('kelas.index')->with('success', 'Data Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response = Http::withToken(session()->get('tokenUser'))->get(env('REST_API_ENDPOINT').'/api/kelas/'. $id);

        $dataResponse = json_decode($response);

        return view('kelas.edit', [
            'dataKelas' => $dataResponse
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = Http::withToken(session()->get('tokenUser'))->put(env('REST_API_ENDPOINT').'/api/kelas/'. $id, [
            'kode_kelas' => $request->kode_kelas,
            'nama_kelas' => $request->nama_kelas,
        ]);

        $dataResponse = json_decode($response);
        if($dataResponse->status == false){
            foreach($dataResponse->message as $msg){
                return redirect()->route('kelas.edit')->with('warning', $msg);
            }
        }

        return redirect('kelas')->with('success', 'Data Berhasil Diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Http::withToken(session()->get('tokenUser'))->delete(env('REST_API_ENDPOINT').'/api/kelas/'.$id);

        return redirect('kelas')->with('success', 'Data Berhasil Dihapus');
    }
}
