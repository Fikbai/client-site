<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    function __construct() {
        $this->middleware(function ($request, $next) {
            // validation
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = Http::withToken(session()->get('tokenUser'))
                        ->get(env('REST_API_ENDPOINT').'/api/users');

        $dataResponse = json_decode($response);

        return view('user.index', [
            'dataUser' => $dataResponse
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $response = Http::withToken(session()->get('tokenUser'))
                        ->get(env('REST_API_ENDPOINT').'/api/users');

        $dataResponse = json_decode($response);

        return view('user.create', [
            'dataUser' => $dataResponse
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = Http::withToken(session()->get('tokenUser'))->post(env('REST_API_ENDPOINT').'/api/users', [
            'type' => $request->type,
            'username' => $request->username,
            'password' => $request->password
        ]);
        $dataResponse = json_decode($response);
        // dd($dataResponse);
        if($dataResponse->status == false){
            foreach($dataResponse->message as $message){
                return redirect()->route('users.create')->with('warning', $message);
            }
        } else {
            return redirect()->route('users.index')->with('success','Data Berhasil Ditambahkan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response = Http::withToken(session()->get('tokenUser'))->get(env('REST_API_ENDPOINT').'/api/users/'.$id);
        $dataResponse = json_decode($response);
        
        return view('user.edit', [
            'dataUser' => $dataResponse,
            'type' => ['admin','guru']
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = Http::withToken(session()->get('tokenUser'))->put(env('REST_API_ENDPOINT').'/api/users/'.$id, [
            'type' => $request->type,
            'username' => $request->username,
            'password' => $request->password
        ]);

        $dataResponse = json_decode($response);
        if($dataResponse->status == true){
            return redirect()->route('users.index')->with('success','Data Berhasil Diubah');
        } else {
            foreach($dataResponse->message as $message){
                return redirect()->route('users.edit', $id)->with('warning', $message);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Http::withToken(session()->get('tokenUser'))->delete(env('REST_API_ENDPOINT').'/api/users/'.$id);

        return redirect('users')->with('success', 'Data Berhasil dihapus');
    }
}
