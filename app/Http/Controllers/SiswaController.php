<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = Http::withToken(session()->get('tokenUser'))
        ->get(env('REST_API_ENDPOINT').'/api/siswa');

        $dataResponse = json_decode($response);

        return view('siswa.index', [
            'dataSiswa' => $dataResponse
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $response = Http::withToken(session()->get('tokenUser'))->get(env('REST_API_ENDPOINT').'/api/siswa');
        $dataResponse = json_decode($response);

        $idkelas = Http::withToken(session()->get('tokenUser'))->get(env('REST_API_ENDPOINT').'/api/kelas');
        $idkelasResponse = json_decode($idkelas);

        return view('siswa.create', [
            'dataSiswa' => $dataResponse,
            'dataKelas' => $idkelasResponse->data
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = Http::withToken(session()->get('tokenUser'))->post(env('REST_API_ENDPOINT').'/api/siswa', [
            
            'nis' => $request->nis,
            'nama' => $request->nama,
            'gender' => $request->gender,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'email' => $request->email,
            'nama_ortu' => $request->nama_ortu,
            'alamat' => $request->alamat,
            'phone_number' => $request->phone_number,
            'kelas_id' => $request->kelas_id,
        ]);

        $dataResponse = json_decode($response);
        if($dataResponse->status == false){
            foreach($dataResponse->message as $message){
                return redirect()->route('siswa.create')->with('warning', $message);
            }
        }

        return redirect()->route('siswa.index')->with('success', 'Data Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response = Http::withToken(session()->get('tokenUser'))->get(env('REST_API_ENDPOINT').'/api/siswa/'.$id);
        $dataResponse = json_decode($response);

        $idkelas = Http::withToken(session()->get('tokenUser'))->get(env('REST_API_ENDPOINT').'/api/kelas');
        $idkelasResponse = json_decode($idkelas);
        return view('siswa.edit', [
            'dataSiswa' => $dataResponse,
            'dataKelas' => $idkelasResponse->data,
            'genders' => ['laki-laki', 'perempuan']
        ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = Http::withToken(session()->get('tokenUser'))->put(env('REST_API_ENDPOINT').'/api/siswa/'.$id, [
            
            'nis' => $request->nis,
            'nama' => $request->nama,
            'gender' => $request->gender,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'email' => $request->email,
            'nama_ortu' => $request->nama_ortu,
            'alamat' => $request->alamat,
            'phone_number' => $request->phone_number,
            'kelas_id' => $request->kelas_id,
        ]);

        $dataResponse = json_decode($response);
        if($dataResponse->status == false){
            foreach($dataResponse->message as $message){
                return redirect()->route('siswa.edit',  $id)->with('warning', $message);
            }
        }

        return redirect()->route('siswa.index')->with('success', 'Data Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Http::withToken(session()->get('tokenUser'))->delete(env('REST_API_ENDPOINT').'/api/siswa/'.$id);

        return redirect('siswa')->with('success', 'Data Berhasil dihapus');
    }
}
