<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Mapel;
use Illuminate\Support\Facades\Http;

class MapelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = Http::withToken(session()->get('tokenUser'))->get(env('REST_API_ENDPOINT').'/api/mapel');

        $dataResponse = json_decode($response);

        return view('mapel.index', [
            'dataMapel' => $dataResponse
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $response = Http::withToken(session()->get('tokenUser'))->get(env('REST_API_ENDPOINT').'/api/mapel');

        $dataResponse = json_decode($response);

        return view('mapel.create', [
            'dataMapel' => $dataResponse
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = Http::withToken(session()->get('tokenUser'))->post(env('REST_API_ENDPOINT').'/api/mapel', [
            'kode_mapel' => $request->kode_mapel,
            'nama_mapel' => $request->nama_mapel,
        ]);

        $dataResponse = json_decode($response);
        if($dataResponse->status == false){
            return redirect('mapel')->with('danger', $dataResponse->message->phone_number[0]);
        }

        return redirect('mapel')->with('success', 'Data Added Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response = Http::withToken(session()->get('tokenUser'))->get(env('REST_API_ENDPOINT').'/api/mapel/'.$id);

        $dataResponse = json_decode($response);

        return view('mapel.edit', [
            'dataMapel' => $dataResponse
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = Http::withToken(session()->get('tokenUser'))->put(env('REST_API_ENDPOINT').'/api/mapel/'.$id, [
            'kode_mapel' => $request->kode_mapel,
            'nama_mapel' => $request->nama_mapel,
        ]);

        $dataResponse = json_decode($response);
        if($dataResponse->status == false){
            return redirect('mapel')->with('danger', $dataResponse->message->phone_number[0]);
        }

        return redirect('mapel')->with('success', 'Data Added Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Http::withToken(session()->get('tokenUser'))->delete(env('REST_API_ENDPOINT').'/api/mapel/'.$id);

        return redirect('mapel')->with('success', 'Data Berhasil dihapus');
    }
}
