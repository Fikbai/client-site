<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */    
    public function index()
    {
        if (Auth::check()) {
            return redirect()->route('dashboard');
        }
        return redirect()->route('login');
    }

    public function dashboard()
    {   
        $guru = Http::withToken(session()->get('tokenUser'))->get(env('REST_API_ENDPOINT').'/api/guru');
        $guruResponse = json_decode($guru);
        
        $kelas = Http::withToken(session()->get('tokenUser'))->get(env('REST_API_ENDPOINT').'/api/kelas');
        $kelasResponse = json_decode($kelas);
        
        $siswa = Http::withToken(session()->get('tokenUser'))->get(env('REST_API_ENDPOINT').'/api/siswa');
        $siswaResponse = json_decode($siswa);
        
        $mapel = Http::withToken(session()->get('tokenUser'))->get(env('REST_API_ENDPOINT').'/api/mapel');
        $mapelResponse = json_decode($mapel);
        
        $jadwal = Http::withToken(session()->get('tokenUser'))->get(env('REST_API_ENDPOINT').'/api/jadwal');
        $jadwalResponse = json_decode($jadwal);

        // dd(session()->get('userLogged'), $guruResponse->data);
        return view('admin.dashboard',[
            'guru' => $guruResponse,
            'kelas' => $kelasResponse,
            'siswa' => $siswaResponse,
            'mapel' => $mapelResponse,
            'jadwal' => $jadwalResponse,
        ]);
    }

    public function not_found()
    {
        return view('not_found');
    }
}
