<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PresensiController extends Controller
{
    public function index() {
        $response = Http::withToken(session()->get('tokenUser'))
        ->get(env('REST_API_ENDPOINT').'/api/siswa');

        $dataResponse = json_decode($response);

        return view('presensi.index', [
            'dataSiswa' => $dataResponse
        ]);
    }
}

?>