<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Jadwal;
use App\Model\Kelas;
use App\Model\Mapel;
use App\Model\Account;
use App\Model\TahunAjaran;
use App\Model\Semester;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;

class JadwalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {               
        $responseJadwal = Http::withToken(session()->get('tokenUser'))->get(env('REST_API_ENDPOINT').'/api/jadwal');
        $dataResponseJadwal = json_decode($responseJadwal);
        
        // dd($dataResponseJadwal->data, session()->get('userLogged'));
        // for($i = 0; $i < $dataResponseJadwal->data; $i++){
        //     if(session()->get('userLogged')->type == "guru" && session()->get('userLogged')->id == $dataResponseJadwal->data[$i]->user_id){
        //     }
        // }
        return view('jadwal.index', [
            'dataJadwal' => $dataResponseJadwal,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $responseKelas = Http::withToken(session()->get('tokenUser'))->get(env('REST_API_ENDPOINT').'/api/kelas/');
        $responseMapel = Http::withToken(session()->get('tokenUser'))->get(env('REST_API_ENDPOINT').'/api/mapel/');
        $responseGuru = Http::withToken(session()->get('tokenUser'))->get(env('REST_API_ENDPOINT').'/api/guru/');

        $dataResponseGuru = json_decode($responseGuru);
        $dataResponseMapel = json_decode($responseMapel);
        $dataResponseKelas = json_decode($responseKelas);

        return view('jadwal.create',[
            'dataJadwal' => $this->index()->dataJadwal,
            'dataGuru' => $dataResponseGuru->data,
            'dataMapel' => $dataResponseMapel->data,
            'dataKelas' => $dataResponseKelas->data,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = Http::withToken(session()->get('tokenUser'))->post(env('REST_API_ENDPOINT').'/api/jadwal', [
            'kelas_id' => $request->kelas_id,
            'mapel_id' => $request->mapel_id,
            'guru_id' => $request->guru_id,
            'hari' => $request->hari,
            'jam_pelajaran' => $request->jam_pelajaran,
        ]);

        $dataResponse = json_decode($response);
        if($dataResponse->status == false){
            foreach($dataResponse->message as $msg){
                return redirect()->route('guru.edit')->with('warning', $msg);
            }
        }

        return redirect('jadwal')->with('success', 'Data Added Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $responseJadwal = Http::withToken(session()->get('tokenUser'))->get(env('REST_API_ENDPOINT').'/api/jadwal/'.$id);

        $dataResponseJadwal = json_decode($responseJadwal);
        return view('jadwal.index', [
            'dataJadwal' => $dataResponseJadwal,
        ]); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $responseKelas = Http::withToken(session()->get('tokenUser'))->get(env('REST_API_ENDPOINT').'/api/kelas/');
        $responseMapel = Http::withToken(session()->get('tokenUser'))->get(env('REST_API_ENDPOINT').'/api/mapel/');
        $responseGuru = Http::withToken(session()->get('tokenUser'))->get(env('REST_API_ENDPOINT').'/api/guru/');

        $dataResponseGuru = json_decode($responseGuru);
        $dataResponseMapel = json_decode($responseMapel);
        $dataResponseKelas = json_decode($responseKelas);

        return view('jadwal.edit',[
            'dataJadwal' => $this->show($id)->dataJadwal,
            'dataGuru' => $dataResponseGuru->data,
            'dataMapel' => $dataResponseMapel->data,
            'dataKelas' => $dataResponseKelas->data,
            'dataHari' => ['senin','selasa','rabu','kamis','jumat','sabtu']
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = Http::withToken(session()->get('tokenUser'))->put(env('REST_API_ENDPOINT').'/api/jadwal/'.$id, [
            'kelas_id' => $request->kelas_id,
            'mapel_id' => $request->mapel_id,
            'guru_id' => $request->guru_id,
            'hari' => $request->hari,
            'jam_pelajaran' => $request->jam_pelajaran,
        ]);

        $dataResponse = json_decode($response);
        if($dataResponse->status == false){
            foreach($dataResponse->message as $msg){
                return redirect()->route('guru.edit')->with('warning', $msg);
            }
        }

        return redirect('jadwal')->with('success', 'Data Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $responseJadwal = Http::withToken(session()->get('tokenUser'))->delete(env('REST_API_ENDPOINT').'/api/jadwal/'.$id);

        $dataResponseJadwal = json_decode($responseJadwal);

        return redirect()->route('jadwal.index')-> with('success', 'Data Berhasil Dihapus'); 
    }
}
