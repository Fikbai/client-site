<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;

class GuruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = Http::withToken(session()->get('tokenUser'))
                        ->get(env('REST_API_ENDPOINT').'/api/guru');

        $dataResponse = json_decode($response);

        return view('guru.index', [
            'dataGuru' => $dataResponse
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $response = Http::withToken(session()->get('tokenUser'))->get(env('REST_API_ENDPOINT').'/api/user-untuk-guru');
        $dataResponse = json_decode($response);
        return view('guru.create', [
            'dataGuru' => $dataResponse
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd();
        $response = Http::withToken(session()->get('tokenUser'))->post(env('REST_API_ENDPOINT').'/api/guru', [
            'user_id' => $request->user_id,
            'nip' => $request->nip,
            'nama' => $request->nama,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'gender' => $request->gender,
            'phone_number' => $request->phone_number,
            'email' => $request->email,
            'alamat' => $request->alamat,
            'pendidikan' => $request->pendidikan,
        ]);

        $dataResponse = json_decode($response);
        if($dataResponse->status == false){
            foreach($dataResponse->message as $msg){
                return redirect()->route('guru.create')->with('warning', $msg);
            }
        }

        return redirect()->route('guru.index')->with('success', 'Data Berhasil Ditambahkan');
    }

    public function account_profile(Request $request)
    {
        //
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->data['guru'] = Account::findOrFail($id);

        return view('account.show', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response = Http::withToken(session()->get('tokenUser'))->get(env('REST_API_ENDPOINT').'/api/guru/'.$id);
        $dataResponse = json_decode($response);
        return view('guru.edit', [
            "dataGuru" => $dataResponse
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = Http::withToken(session()->get('tokenUser'))->put(env('REST_API_ENDPOINT').'/api/guru/'.$id, [
            'user_id' => $request->user_id,
            'nip' => $request->nip,
            'nama' => $request->nama,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'gender' => $request->gender,
            'phone_number' => $request->phone_number,
            'email' => $request->email,
            'alamat' => $request->alamat,
            'pendidikan' => $request->pendidikan,
        ]);

        $dataResponse = json_decode($response);
        if($dataResponse->status == false){
            foreach($dataResponse->message as $msg){
                return redirect()->route('guru.edit')->with('warning', $msg);
            }
        } else {
            return redirect()->route('guru.index')->with('success', 'Data Berhasil Diedit');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Http::withToken(session()->get('tokenUser'))->delete(env('REST_API_ENDPOINT').'/api/guru/'.$id);

        return redirect()->route('guru.index')->with('success', 'Data Deleted Successfully!');
    }

    public function account()
    {
        return view('account.account');
    }

    public function account_save(Request $request)
    {
        $this->validate($request,[
            'currentPassword' => 'required',
            'password' => 'required|string|min:6|confirmed'
        ]);
        
        $user = Auth::user();
        $cekPassword = Hash::check($request->currentPassword, $user->password);
        if (!$cekPassword) {
            return redirect()->route('account.index')->with('danger','Current password does not match!');
        }
        
        $user->password = Hash::make($request->password);
        $user->save();

        return redirect()->route('account.index')->with('success', 'Your password changed successfully!');
    }
}
