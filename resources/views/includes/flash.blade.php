@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    {{ $message }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if ($message = Session::get('danger'))
@foreach ($message as $word)
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    {{ $word }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endforeach
@endif

@if ($message = Session::get('warning'))
@foreach ($message as $word)
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    {{ $word }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endforeach
@endif

@if ($message = Session::get('info'))
<div class="alert alert-info">{{$message }}</div>
@endif

@if ($errors->any())
<div class="alert alert-danger">
    @foreach ($errors->all() as $error)
    <ul>
        <li>{{ $error }}</li>
    </ul>
    @endforeach
</div>
@endif

@if (Session::has('validationErrors'))
<div class="alert alert-danger">
    @foreach (Session::get('validationErrors') as $item)
    @foreach ($item as $err)
    <ul>
        <li>{{ $err }}</li>
    </ul>
    @endforeach
    @endforeach
</div>
@endif