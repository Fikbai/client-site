@extends('layouts/main-admin')

@section('title', 'Edit Siswa')

@section('container')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h2 class="m-0 text-dark">SISWA</h2>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active">Edit Siswa</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<section class="container-fluid">
    <div class="card">
        <div class="card-body">
            @include ('includes.flash')
            <form role="form" method="post" action="{{ route('siswa.update', $dataSiswa->data->id) }}">
                @csrf
                @method('put')
                <div class="card-body">
                    <h4>Data Siswa</h4>
                    <div class="form-group">
                        <label for="exampleInputJK">Kelas</label>
                        <select class="form-control" name="kelas_id" id="kelas_id" required>
                            @foreach ($dataKelas as $data)
                            @if ($data->id == $dataSiswa->data->kelas_id)
                            <option value="{{ $data->id }}">{{ $data->nama_kelas }}</option>
                            @else
                            <option value="{{ $data->id }}">{{ $data->nama_kelas }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">NIS</label>
                        <input type="text" class="form-control" name="nis" id="nis" value="{{ $dataSiswa->data->nis }}"
                            required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Nama</label>
                        <input type="text" class="form-control" name="nama" id="nama"
                            value="{{ $dataSiswa->data->nama }}" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tempat Lahir</label>
                        <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir"
                            value="{{ $dataSiswa->data->tempat_lahir }}" required>
                    </div>
                    <div class="form-group mb-5">
                        <label for="exampleInputPendidikanTerakhir">Tanggal Lahir</label>
                        <input type="text" class="form-control pull-right" id="datepicker" name="tgl_lahir"
                            value="{{ date('d/m/Y' , strtotime($dataSiswa->data->tgl_lahir)) }}" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputJK">Jenis Kelamin</label>
                        <select class="form-control" name="gender" id="gender" required>
                            @foreach ($genders as $gender)
                            @if ($gender == $dataSiswa->data->gender)
                            <option value="{{ $gender }}">{{ ucfirst( $gender) }}</option>
                            @else
                            <option value="{{ $gender }}">{{ ucfirst( $gender) }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPendidikanTerakhir">Nomor Telphone</label>
                        <input id="phone_number" type="number"
                            class="form-control form-control-user @error('phone_number') is-invalid @enderror"
                            name="phone_number" id="phoneValidation" value="{{ $dataSiswa->data->phone_number }}"
                            required style="-webkit-appearance: none;margin: 0;">
                        @error('phone_number')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Email</label>
                        <input type="email" class="form-control" name="email" id="email"
                            value="{{ $dataSiswa->data->email }}" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Nama Orang Tua</label>
                        <input type="text" class="form-control" name="nama_ortu" id="nama_ortu"
                            value="{{ $dataSiswa->data->nama_ortu }}" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Alamat</label>
                        <textarea name="alamat" class="form-control" id="alamat" cols="30" rows="10"
                            value="{{ $dataSiswa->data->alamat }}" required></textarea>
                    </div>
                    <div class="card-body">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@include ('includes.scripts')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
    integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $(function () {
        //Date picker
        $('#datepicker').datepicker({
        autoclose: true
        })
    });
    $('#phoneValidation').on('keyup', function () {
        this.value = this.value.replace(/[^0-9]/gi, '')
      });
</script>
@endsection