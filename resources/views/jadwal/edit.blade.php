@extends('layouts/main-admin')

@section('title', 'Edit Jadwal')

@section('container')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h2 class="m-0 text-dark">JADWAL</h2>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active">Edit Jadwal</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<section class="container-fluid">
    <div class="card">
        <div class="card-body">
            @include ('includes.flash')
            <form role="form" method="post" action="{{ route('jadwal.update', $dataJadwal->data->id) }}"
                enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputJK">Kelas</label>
                        <select class="form-control" name="kelas_id" id="kelas_id" required>
                            @foreach ($dataKelas as $data)
                            @if ($data->id == $dataJadwal->data->kelas_id)
                            <option value="{{ $data->id }}" selected>{{ $data->nama_kelas }}</option>
                            @else
                            <option value="{{ $data->id }}">{{ $data->nama_kelas }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputJK">Mata Pelajaran</label>
                        <select class="form-control" name="mapel_id" id="mapel_id" required>
                            @foreach ($dataMapel as $data)
                            @if ($data->id == $dataJadwal->data->mapel_id)
                            <option value="{{ $data->id }}" selected>{{ $data->nama_mapel }}</option>
                            @else
                            <option value="{{ $data->id }}">{{ $data->nama_mapel }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputJK">Guru Pengampu</label>
                        <select class="form-control" name="guru_id" id="guru_id" required>
                            @foreach ($dataGuru as $data)
                            @if ($data->id == $dataJadwal->data->guru_id)
                            <option value="{{ $data->id }}" selected>{{ $data->nama }}</option>
                            @else
                            <option value="{{ $data->id }}">{{ $data->nama }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputJK">Hari</label>
                        <select class="form-control" name="hari" id="hari" required>
                            @foreach ($dataHari as $hari)
                            @if ($hari == $dataJadwal->data->hari)
                            <option value="{{ $hari }}" selected>{{ ucfirst($hari) }}</option>
                            @endif
                            <option value="{{ $hari }}">{{ ucfirst($hari) }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Jam Pelajaran</label>
                        <input type="text" class="form-control" name="jam_pelajaran" id="jam_pelajaran"
                            value="{{ $dataJadwal->data->jam_pelajaran }}" required>
                    </div>
                    <div class="card-body">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@include ('includes.scripts')
@endsection