@extends('layouts/main-admin')

@section('title', 'Jadwal')

@section('container')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h2 class="m-0 text-dark">JADWAL</h2>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active">Jadwal</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<section class="container-fluid">
    <div class="card">
        <div class="card-body">
            @include ('includes.flash')

            <div class="mb-4">
                @foreach ($dataJadwal->data as $jadwal)
                    <p class="text-danger"><b>Guru</b> : {{ $jadwal->nama }}</p>
                    <p class="text-warning"><b>NIP</b> : {{ $jadwal->nip }}</p>
                    <p class="text-success"><b>Email</b> : {{ $jadwal->email }}</p>
                    @break
                @endforeach
            </div>

            <table id="data-admin" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="40">NO</th>
                        <th>MAPEL</th>
                        <th>KELAS</th>
                        <th>HARI</th>
                        <th>JAM PELAJARAN</th>
                        <th width="120">AKSI</th>
                    </tr>
                </thead>
                <tbody>
                    @if ($dataJadwal->data != null)
                    @foreach ($dataJadwal->data as $key => $jadwal)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $jadwal->nama_mapel }}</td>
                        <td>{{ $jadwal->nama_kelas }}</td>
                        <td>{{ ucfirst($jadwal->hari) }}</td>
                        <td>{{ $jadwal->jam_pelajaran }}</td>
                        <td class="text-center">
                            <a href="{{ route('presensi.index') }}">
                                <button class="btn btn-primary" data-toggle="tooltip" data-placement="top"
                                    title="Presensi">
                                    <i class="bi bi-check2-square"></i>
                                </button>
                            </a>

                            <a href="{{ route('jadwal.edit', $jadwal->id) }}">
                                <button class="btn btn-secondary" data-toggle="tooltip" data-placement="top"
                                    title="Ubah">
                                    <i class="fa fa-edit"></i>
                                </button>
                            </a>

                            <form action="/jadwal/{{ $jadwal->id }}" id="delete-jadwal-{{ $jadwal->id }}" method="post"
                                style="display: inline">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</section>
@include ('includes.scripts')
<script type="text/javascript">
    $(document).ready(function(){
            $("#data-admin_length").append('<a  href="{{ route('jadwal.create') }}"> <button type="button" class="btn btn-outline-primary ml-3">Tambah</button></a>');
        });
</script>
@endsection