@extends('layouts/main-admin')

@section('title', 'Rekap Presensi')

@section('container')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h2 class="m-0 text-dark">Rekap Presensi</h2>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active">Siswa</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<section class="container-fluid">
    <div class="card">
        <div class="card-body">
            @include ('includes.flash')
            <table id="data-admin" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="40">NO</th>
                        <th>NIS</th>
                        <th>NAMA</th>
                        <th width="120">PRESENSI</th>
                    </tr>
                </thead>
                <tbody>
                    @if ($dataSiswa->data != null)
                    @foreach ($dataSiswa->data as $key => $siswa)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $siswa->nis }}</td>
                        <td>{{ $siswa->nama }}</td>
                        <td class="text-center">
                            <form id="" action="" method="post" style="display: inline;">
                                @csrf
                                    <input type="checkbox" name="presensi" value="Present">
                                    <input type="checkbox" name="presensi">
                                    <input type="checkbox" name="presensi">
                                    <input type="checkbox" name="presensi">
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</section>
@include ('includes.scripts')
<script type="text/javascript">
    $(document).ready(function(){
            $("#data-admin_length").append('<a  href="{{ route('siswa.create') }}"> <button type="button" class="btn btn-outline-primary ml-3">Tambah</button></a>');
        });
</script>
@endsection